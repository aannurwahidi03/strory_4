from django.apps import AppConfig


class MyProfileConfig(AppConfig):
    name = 'My_Profile'
