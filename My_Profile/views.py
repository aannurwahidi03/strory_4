from django.shortcuts import render

def homepage(request):
    return render(request, 'homepage.html', {})

def contact(request):
    return render(request, 'contact.html', {})

def aboutme(request):
    return render(request, 'aboutme.html', {})

def skills(request):
    return render(request, 'skills.html', {})

def new_page(request):
    return render(request, 'new_page.html', {})