from django.urls import path
from .views import homepage, contact, aboutme, skills, new_page

app_name = "Story_4"

urlpatterns = [
	path('', homepage, name="homepage"),
     path('about', aboutme, name="aboutme"),
     path('skill', skills, name="skills"),
     path('contact', contact, name="contact"),
     path('new', new_page, name="new_page")
]